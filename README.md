Spring Boot, H2 db, Gradle

```
gradle build
java -jar ./build/libs/*.jar
```

runs on localhost:8080
________

Book: 
```
{
  "id" : <long int>
  "name" : <string>,
  "summary" : <string>,
  "authors" : [ {"firstName" : <string>, "lastName" : <string>} ]
}
```
```
/books/{id} (GET, PUT, DELETE)
/books (GET, POST)

Search by title
/books?title=some_title (GET)

Search by author's name (strict)
/books/author?firstName=John&lastName=Smith&strict=1 (GET)

If author is not found, show for all similar
/books/author?firstName=John&lastName=Smith (GET)
```