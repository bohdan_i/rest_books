package my.bohdan_i.rest_books.repository;

import my.bohdan_i.rest_books.model.Book;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import java.util.List;

@RepositoryRestResource(exported = false)
public interface BookRepository extends JpaRepository<Book, Long> {
    List<Book> findByNameLike(@Param("name") String name);
}
