package my.bohdan_i.rest_books.repository;

import my.bohdan_i.rest_books.model.Author;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import java.util.List;

@RepositoryRestResource(exported = false)
public interface AuthorRepository extends JpaRepository<Author, Long> {
    Author findByFirstNameAndLastNameAllIgnoreCase(@Param("firstName") String firstName, @Param("lastName") String lastName);
    List<Author> findByFirstNameLikeAndLastNameLikeAllIgnoreCase(@Param("firstName") String firstName, @Param("lastName") String lastName);
}
