package my.bohdan_i.rest_books.dto;

import org.hibernate.validator.constraints.NotEmpty;

public class AuthorDto {
    @NotEmpty
    public String firstName;
    @NotEmpty
    public String lastName;
    public AuthorDto() {}
    public AuthorDto(String firstName, String lastName) {
        this.firstName = firstName;
        this.lastName = lastName;
    }
}
