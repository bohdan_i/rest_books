package my.bohdan_i.rest_books.dto;

import org.hibernate.validator.constraints.NotEmpty;
import javax.validation.Valid;
import java.util.Set;

public class BookDto {
    public long id;
    @NotEmpty(message = "Book's name is required")
    public String name;
    @NotEmpty(message = "Book must have a summary")
    public String summary;
    @NotEmpty(message = "Book must have at least one author")
    @Valid
    public Set<AuthorDto> authors;
    public BookDto() {}
    public BookDto(long id, String name, String summary, Set<AuthorDto> authors)  {
        this.id = id;
        this.name = name;
        this.summary = summary;
        this.authors = authors;
    }
}
