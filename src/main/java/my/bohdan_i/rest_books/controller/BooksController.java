package my.bohdan_i.rest_books.controller;

import my.bohdan_i.rest_books.dto.AuthorDto;
import my.bohdan_i.rest_books.dto.BookDto;
import my.bohdan_i.rest_books.model.Author;
import my.bohdan_i.rest_books.model.Book;
import my.bohdan_i.rest_books.repository.AuthorRepository;
import my.bohdan_i.rest_books.repository.BookRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.validation.Valid;
import javax.websocket.server.PathParam;
import java.net.URI;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/books")
public class BooksController {

    private final BookRepository bookRepository;

    private final AuthorRepository authorRepository;

    @Autowired
    public BooksController(BookRepository bookRepository, AuthorRepository authorRepository) {
        this.bookRepository = bookRepository;
        this.authorRepository = authorRepository;
    }

    // CRUD

    @PostMapping
    public ResponseEntity<Object> createBook(@Valid @RequestBody BookDto bookDto) {
        Set<Author> authors = bookDto.authors.stream()
                .map((AuthorDto ad) -> {
                    Author a = authorRepository.findByFirstNameAndLastNameAllIgnoreCase(ad.firstName, ad.lastName);
                    if (a == null) {
                        a = new Author(ad.firstName, ad.lastName);
                        authorRepository.save(a);
                    }
                    return a;
                })
                .collect(Collectors.toSet());
        Book b = new Book(bookDto.name, bookDto.summary, authors);
        b = bookRepository.save(b);

        URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}")
                .buildAndExpand(b.getId()).toUri();

        return ResponseEntity.created(location).build();
    }

    @GetMapping("{id}")
    public ResponseEntity<BookDto> getBook(@PathVariable("id") long id) {
        return Optional
                .ofNullable(bookRepository.findOne(id))
                .map(book -> ResponseEntity.ok().body(bookToDto(book)))
                .orElseGet(() -> ResponseEntity.notFound().build());
    }

    @DeleteMapping("{id}")
    public ResponseEntity<Object> deleteBook(@PathVariable("id") long id) {
        try {
            List<Long> authorIds = bookRepository.findOne(id).getAuthors().stream().map(Author::getId).collect(Collectors.toList());
            bookRepository.delete(id);
            for (long a : authorIds) {
                removeAuthorIfUnusedById(a);
            }
            return ResponseEntity.noContent().build();
        } catch (Exception e) {
            return ResponseEntity.notFound().build();
        }
    }

    @PutMapping("{id}")
    public ResponseEntity<Object> updateBook(@PathVariable("id") long id,
                                             @Valid @RequestBody BookDto bookDto) {
        Book b = bookRepository.findOne(id);
        if (b == null)
            return ResponseEntity.notFound().build();

        b.setName(bookDto.name);
        b.setSummary(bookDto.summary);
        Set<Author> oldAuthors = b.getAuthors();
        Set<Author> updatedAuthors = bookDto.authors.stream()
                .map((AuthorDto ad) -> {
                    // is this author already in the authors set?
                    for (Author a : oldAuthors)
                        if (a.getFirstName().equals(ad.firstName)
                                && a.getLastName().equals(ad.lastName))
                            return a;
                    // or is it already saved?
                    Author a = authorRepository.findByFirstNameAndLastNameAllIgnoreCase(ad.firstName, ad.lastName);

                    if (a == null) {
                        a = new Author(ad.firstName, ad.lastName);
                        authorRepository.save(a);
                    }

                    return a;
                })
                .collect(Collectors.toSet());
        b.setAuthors(updatedAuthors);
        b = bookRepository.save(b);

        // If there are authors without any book
        if (oldAuthors.removeAll(updatedAuthors)) {
            for (Author a : oldAuthors)
                removeAuthorIfUnusedById(a.getId());
        }

        return new ResponseEntity<>(bookToDto(b), HttpStatus.OK);
    }

    // SEARCH

    @GetMapping
    public List<BookDto> searchBook(@PathParam("title") String title) {
        List<Book> books = title == null
                ? bookRepository.findAll()
                : bookRepository.findByNameLike("%" + title + "%");

        return books.stream()
                .map(this::bookToDto)
                .collect(Collectors.toList());
    }

    @GetMapping("/author")
    public List<BookDto> searchByAuthor(
            @PathParam("firstName") String firstName,
            @PathParam("lastName") String lastName,
            @PathParam("strict") boolean strict) {
        if (firstName == null) firstName = "";
        if (lastName == null) lastName = "";

        Author author = authorRepository.findByFirstNameAndLastNameAllIgnoreCase(firstName, lastName);
        if (author != null) {
            return author.getBooks().stream()
                    .map(this::bookToDto)
                    .collect(Collectors.toList());
        }

        if (strict) return new ArrayList<>();

        List<Author> authors = authorRepository.findByFirstNameLikeAndLastNameLikeAllIgnoreCase(
                "%" + firstName + "%",
                "%" + lastName + "%"
        );

        return authors.stream()
                .flatMap((Author a) -> a.getBooks().stream())
                .distinct()
                .map(this::bookToDto)
                .collect(Collectors.toList());
    }

    private BookDto bookToDto(Book b) {
        Set<AuthorDto> authorDtos = b.getAuthors().stream()
                .map((Author a) -> new AuthorDto(a.getFirstName(), a.getLastName()))
                .collect(Collectors.toSet());

        return new BookDto(b.getId(), b.getName(), b.getSummary(), authorDtos);
    }

    private void removeAuthorIfUnusedById(long id) {
        Author a = authorRepository.findOne(id);
        if (a.getBooks().size() == 0)
            authorRepository.delete(a.getId());
    }
}
